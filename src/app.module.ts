import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common'
import { LoggerModule } from 'nestjs-pino'
import { IncomingMessage } from 'http'
import { CorrelationIdMiddleware } from '@middlewares/correlation-id.middleware'
import { ConfigModule } from '@nestjs/config'
import { APP_GUARD } from '@nestjs/core'
import { AuthenticationGuard } from '@guards/authentication.guard'
import { JwtModule } from '@nestjs/jwt'
import { AuthorizationGuard } from '@guards/authorization.guard'

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    LoggerModule.forRoot({
      pinoHttp: {
        transport:
          process.env.NODE_ENV === 'local'
            ? {
                target: 'pino-pretty',
                options: {
                  messageKey: 'message',
                },
              }
            : undefined,
        messageKey: 'message',
        customProps: (req: IncomingMessage) => ({ correlationId: req['x-correlation-id'] }),
        autoLogging: false,
        serializers: {
          req: () => undefined,
          res: () => undefined,
        },
        base: undefined,
        formatters: {
          level: (label) => ({ level: label.toUpperCase() }),
        },
      },
    }),
    JwtModule.register({
      secret: process.env.JWT_SECRET,
    }),
  ],
  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthenticationGuard,
    },
    {
      provide: APP_GUARD,
      useClass: AuthorizationGuard,
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(CorrelationIdMiddleware).forRoutes('*')
  }
}
