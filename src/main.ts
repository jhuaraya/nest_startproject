import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { Logger } from 'nestjs-pino'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import { LogerInterceptor } from '@interceptors/logger-interceptor'
import { ValidationPipe } from '@nestjs/common'

// eslint-disable-next-line @typescript-eslint/no-var-requires
const packageData = require('../package.json')
async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  app.useLogger(app.get(Logger))
  app.useGlobalInterceptors(new LogerInterceptor())
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
    })
  )
  // app.useGlobalGuards(AuthenticationGuard)
  if (['dev', 'qa', 'local'].includes(process.env.NODE_ENV)) {
    const config = new DocumentBuilder()
      .setTitle(packageData.name)
      .setDescription(packageData.description)
      .setVersion(packageData.version)
      .addBearerAuth(
        {
          type: 'http',
          scheme: 'bearer',
        },
        'bearer'
      )
      .build()
    const document = SwaggerModule.createDocument(app, config)
    SwaggerModule.setup('docs', app, document)
  }
  await app.listen(3000)
}
bootstrap()
