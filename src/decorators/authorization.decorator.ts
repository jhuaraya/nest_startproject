import { SetMetadata, applyDecorators } from '@nestjs/common'
import { ApiSecurity } from '@nestjs/swagger'
import { AUTH, ROLES } from '@shared/auth.constants'
import { Authentication } from './authentication.decorator'

export const HasRole = (roles: ROLES | ROLES[]) => {
  return applyDecorators(SetMetadata(AUTH.NEED_ROLES, roles), ApiSecurity('bearer'), Authentication())
}
