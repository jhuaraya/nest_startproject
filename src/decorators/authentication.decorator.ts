import { SetMetadata, applyDecorators } from '@nestjs/common'
import { ApiSecurity } from '@nestjs/swagger'
import { AUTH } from '@shared/auth.constants'

export const Authentication = () => {
  return applyDecorators(SetMetadata(AUTH.NEED_AUTH, true), ApiSecurity('bearer'))
}
