import { AuthUser } from '@dtos/auth-user.dto'
import { createParamDecorator } from '@nestjs/common'

export const User = createParamDecorator((data, context): AuthUser => {
  const req = context.switchToHttp().getRequest()
  return req.user
})
