import { Injectable, NestMiddleware } from '@nestjs/common'
import { randomUUID } from 'crypto'
import { NextFunction, Response } from 'express'
import { Request } from 'supertest'
export const CORRELATION_ID_HEADER = 'x-correlation-id'
@Injectable()
export class CorrelationIdMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    const id = randomUUID()
    req[CORRELATION_ID_HEADER] = id
    res.set(CORRELATION_ID_HEADER, id)
    next()
  }
}
