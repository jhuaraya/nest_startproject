import { CallHandler, ExecutionContext, Injectable, Logger, NestInterceptor } from '@nestjs/common'
import { tap } from 'rxjs/operators'

@Injectable()
export class LogerInterceptor implements NestInterceptor {
  private logger = new Logger()
  intercept(context: ExecutionContext, next: CallHandler) {
    const now = Date.now()
    const req = context.switchToHttp().getRequest()
    this.logger.log({
      path: req.url,
      method: req.method,
      body: req.body,
      query: req.query,
    })
    return next.handle().pipe(
      tap({
        next: () => this.logger.log(`Request took ${Date.now() - now}ms`),
        error: (err) => {
          this.logger.error({
            stack: err.stack,
            timespend: `${Date.now() - now}ms`,
          })
        },
      })
    )
  }
}
