import { CanActivate, ExecutionContext, Injectable, Logger, UnauthorizedException } from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import { JwtService } from '@nestjs/jwt'
import { AUTH } from '@shared/auth.constants'
import { Observable } from 'rxjs'

@Injectable()
export class AuthenticationGuard implements CanActivate {
  private reflector: Reflector = new Reflector()
  private logger = new Logger(AuthenticationGuard.name)
  constructor(private jwtService: JwtService) {}
  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const need_auth =
      this.reflector.getAllAndOverride<boolean>(AUTH.NEED_AUTH, [context.getClass(), context.getHandler()]) || false
    if (!need_auth) return true
    try {
      const request = context.switchToHttp().getRequest()
      const token = request.headers.authorization.split(' ')[1]
      if (!token) {
        this.logger.error('No se encontró el token en la petición')
        throw new UnauthorizedException({
          message: 'Acceso restringido',
        })
      }
      request.user = this.jwtService.verify(token)
      return true
    } catch (e) {
      this.logger.error('Error al validar el token: ' + e.message)
      throw new UnauthorizedException({
        message: 'Acceso restringido',
      })
    }
  }
}
