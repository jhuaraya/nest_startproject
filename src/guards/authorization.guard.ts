import { CanActivate, ExecutionContext, Injectable, Logger, UnauthorizedException } from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import { JwtService } from '@nestjs/jwt'
import { AUTH, ROLES } from '@shared/auth.constants'
import { Observable } from 'rxjs'

@Injectable()
export class AuthorizationGuard implements CanActivate {
  private reflector: Reflector = new Reflector()
  private logger = new Logger(AuthorizationGuard.name)
  constructor(private jwtService: JwtService) {}
  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    let endpointRoles =
      this.reflector.getAllAndOverride<ROLES[]>(AUTH.NEED_ROLES, [context.getClass(), context.getHandler()]) || []
    if (!endpointRoles || endpointRoles.length == 0) return true
    if (!Array.isArray(endpointRoles)) {
      endpointRoles = [endpointRoles]
    }
    try {
      const request = context.switchToHttp().getRequest()
      let tokenRoles = request.user.roles
      if (!tokenRoles || tokenRoles.length == 0) {
        this.logger.error('No se encontraron roles en el token')
        throw new UnauthorizedException({
          message: 'Acceso restringido',
        })
      }
      if (!Array.isArray(tokenRoles)) {
        tokenRoles = [tokenRoles]
      }
      if (!endpointRoles.some((role) => tokenRoles.includes(role))) {
        this.logger.error('No se encontraron roles en el token')
        this.logger.warn('Roles requeridos endpoint: ' + endpointRoles.join(', '))
        this.logger.warn('Roles del token: ' + tokenRoles.join(', '))
        throw new UnauthorizedException({
          message: 'Acceso restringido',
        })
      }

      return true
    } catch (e) {
      this.logger.error('Validacion de Roles: ' + e.message)
      throw new UnauthorizedException({
        message: 'Acceso restringido',
      })
    }
  }
}
