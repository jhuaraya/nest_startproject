export enum AUTH {
  NEED_AUTH = 'need_auth',
  NEED_ROLES = 'need_roles',
}
export enum ROLES {
  ADMIN = 'admin',
  USER = 'user',
}
