import { ROLES } from '@shared/auth.constants'

export type AuthUser = {
  sub: string
  username: string
  iat: number
  roles: ROLES | ROLES[]
}
